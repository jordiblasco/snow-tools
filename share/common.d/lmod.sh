#!/bin/bash
# These are the common functions which may be used by sNow! Command Line Interface 
# Developed by Jordi Blasco <jordi.blasco@hpcnow.com>
# For more information, visit the official website : www.hpcnow.com/snow
#
function install_lmod()
{
    case $OS in
        debian|ubuntu)
            pkgs="lua lua-filesystem lua-posix liblua5.2-dev"
        ;;
        rhel|redhat|centos)
            pkgs="lua-devel lua-filesystem lua-posix"
        ;;
        suse|sle[sd]|opensuse)
            pkgs="lua lua-devel lua-luafilesystem lua-luaposix"
        ;;
        *)
            warning_msg "This distribution is not supported."
        ;;
    esac
    install_software "$pkgs"
    if is_golden_node; then
        if [[ ! -e $SNOW_SOFT/lmod/lmod/init/profile ]]; then
            chown -R $sNow_USER:$sNow_GROUP $SNOW_SOFT
            cd $SNOW_SOFT
            su $sNow_USER -c "unset https_proxy; git clone https://github.com/TACC/Lmod.git /dev/shm/Lmod; cd /dev/shm/Lmod; ./configure --prefix=$SNOW_SOFT; make; make install"
            rm -fr /dev/shm/Lmod
        fi
    fi
    ln -sf $SNOW_SOFT/lmod/lmod/init/profile /etc/profile.d/lmod.sh
    ln -sf $SNOW_SOFT/lmod/lmod/init/cshrc /etc/profile.d/lmod.csh
} 1>>$LOGFILE 2>&1
